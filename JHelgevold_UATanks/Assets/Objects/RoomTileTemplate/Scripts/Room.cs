﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour {

	public GameObject doorNorth;
	public GameObject doorSouth;
	public GameObject doorEast;
	public GameObject doorWest;

    public GameObject[] enemySpawnPoints;
    public GameObject[] playerSpawnPoints;
    public GameObject[] powerUpSpawnPoints;
    public GameObject[] waypoints;
}
