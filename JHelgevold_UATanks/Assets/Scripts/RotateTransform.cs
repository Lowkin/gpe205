﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTransform : MonoBehaviour {

    public float rotationSpeed = 5f; // how fast do we rotate
    private Transform myTransform; // reference for our transform

	// Use this for initialization
	void Start () {
        myTransform = transform; // set our transform reference
	}
	
	// Update is called once per frame
	void Update () {
        myTransform.Rotate(0, rotationSpeed * Time.deltaTime, 0); // rotate around the y axis by our speed
	}
}
