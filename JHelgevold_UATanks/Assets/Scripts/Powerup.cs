﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup {

    /* Modifier variables */
    public float speedModifier;
    public int healthModifier;
    public int maxHealthModifier;
    public float fireRateModifier;

    public float duration; // duration for powerup

    public bool isPermanent; // indicates if the powerup is permanent

	// Function to call when this powerup is acquired
    public void OnActivate(TankData targetData)
    {
        targetData.forwardSpeed += speedModifier; // add to our forward movement speed
        targetData.hitPoints += healthModifier; // add to our current health
        targetData.maxHitPoints += maxHealthModifier; // add to our max health
        targetData.fireDelay -= fireRateModifier; // subtract from our delay
    }

    // Function to call when this powerup is lost
    public void OnDeactivate(TankData targetData)
    {
        targetData.forwardSpeed -= speedModifier; // remove from our forward movement speed
        targetData.hitPoints -= healthModifier; // remove from our current health
        targetData.maxHitPoints -= maxHealthModifier; // remove from our max health
        targetData.fireDelay += fireRateModifier; // add back to our delay
    }
}
