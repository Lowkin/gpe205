﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public Powerup powerUp; // the powerup that we are
    public AudioClip feedback; // audio clip to play when collected

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Function to handle collision for pickup
    private void OnTriggerEnter(Collider other)
    {
        PowerUpController powCon = other.GetComponent<PowerUpController>(); // the powerup controller of what just hit us

        // if powCon exists
        if (powCon && !other.isTrigger)
        {
            powCon.Add(powerUp); // add ourselves to the list of powerups of whoever picked us up

            if (feedback)
                AudioSource.PlayClipAtPoint(feedback, transform.position, 1f); // play our collection audio

            Destroy(gameObject); // destroy our object since we've been collected
        }
    }
}
