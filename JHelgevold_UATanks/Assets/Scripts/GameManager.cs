﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour {

    public static GameManager gm; // Singleton GameManager object
    public Camera mainCamera; // Reference to our main camera

    public GameObject playerPrefab; // Reference to player prefab
    public TankData playerData; // Reference to the players tank data
    public Transform playerTransform; // Reference to the players transform
    public float playerScore = 0; // Current score the player has achieved

    public GameObject[] enemies; // List of enemy prefabs for map generation

    public Material[] enemyMaterials;


    // declarations for our various layers
    [HideInInspector]
    public int playerLayer;
    [HideInInspector]
    public int enemyLayer;
    [HideInInspector]
    public int projectileLayer;
    [HideInInspector]
    public int obstacleLayer;

    /* Map generation variables */
    public int rows; // how many rows will our map have
    public int cols; // how man columns will our map have
    public int mapSeed; // Seed for the procedural generation of the map

    public enum GenerationMode { Random, Seed, MOTD}; // enum to select what kind of map we want
    public GenerationMode randomMode = GenerationMode.Random; // set our default to random

    private float roomWidth = 50f; // room width
    private float roomHeight = 50f; // room height

    public GameObject[] roomPrefabs; // array of available rooms to build our level with
    public GameObject[] powerUps; // array of powerups spawned

    private Room[,] grid; // reference to hold our room objects
    /***************************/



    private void Awake()
    {
        // If we don't have a GameManager instance set our singleton to this instance
        if (gm == null)
            gm = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

        // Assign our layer variables
        playerLayer = LayerMask.NameToLayer("Player");
        enemyLayer = LayerMask.NameToLayer("Enemy");
        projectileLayer = LayerMask.NameToLayer("Projectile");
        obstacleLayer = LayerMask.NameToLayer("Obstacle");

        // Generate our map
        GenerateGrid();

        // Spawn our player
        SpawnPlayer();
    }

    // Use this for initialization
    void Start () {

      
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // Function to update the players score
    public void UpdatePlayerScore(int score)
    {
        playerScore += score;
    }

    public void GameOver()
    {
        Application.Quit();  // Exit program
    }

    // Function to generate our grid
    public void GenerateGrid()
    {
        grid = new Room[cols, rows]; // set up our grid with our parameters
        switch(randomMode)
        {
            case GenerationMode.MOTD:
                UnityEngine.Random.InitState(DateToInt(DateTime.Now.Date)); // Seed our random number generator with todays date (no time)
                break;
            case GenerationMode.Seed:
                UnityEngine.Random.InitState(mapSeed); // Seed our random number generator with the user provided seed
                break;
            default:
                break; // default will be to break
        }

        // Loop through rows and columsn and generate rooms
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                Vector3 newPosition = new Vector3(roomWidth * j, 0f, roomHeight * i); // location for this tile

                GameObject roomObject = Instantiate(roomPrefabs[UnityEngine.Random.Range(0, roomPrefabs.Length)], newPosition, Quaternion.identity); // Instantiate our room
                roomObject.transform.parent = transform; // Parent this new room to the map generator

                Room tempRoom = roomObject.GetComponent<Room>(); // reference to the room component of our newly instantiated room

                /* Set our doors to inactive so they open */
                if (i < rows - 1)
                    tempRoom.doorNorth.SetActive(false); // set the north door open if we are not in the top row
                if (i > 0)
                    tempRoom.doorSouth.SetActive(false); // set the sout door open if we are not in the bottom row

                if (j < cols - 1)
                    tempRoom.doorEast.SetActive(false); // set the east door open if we are not in the right most column
                if (j > 0)
                    tempRoom.doorWest.SetActive(false); // set the west door open if we are not in the left msot column

                grid[j, i] = tempRoom; // Add a reference to the room component for this new room object to our grid

                roomObject.name = "Room_" + j + ", " + i; // give the game object a name

                PopulateEnemies(tempRoom); // populate our enemies
            }
        }
    }

    // function to populate enemies in our map
    private void PopulateEnemies(Room room)
    {
        int count = UnityEngine.Random.Range(1, room.enemySpawnPoints.Length); // random number of enemies per room
        List<int> spawnPoints = new List<int>(); // list to hold our spawn points
        int temp = 0; // temp count variable

        // populate our spawn points with unique indicies in the spawn point array
        while (temp < count)
        {
            int index = UnityEngine.Random.Range(0, room.enemySpawnPoints.Length); // get a random index into our spawn points

            // if we dont have this index, add it
            if (!spawnPoints.Contains(index))
            {
                spawnPoints.Add(index); // add our spawn point
                temp++; // increment our counter
            }
        }

        // go through our list and spawn an enemy in each spot
        foreach (int point in spawnPoints)
        {
            GameObject enemy = Instantiate(gm.enemies[UnityEngine.Random.Range(0, gm.enemies.Length)], room.enemySpawnPoints[point].transform.position, Quaternion.identity); // spawn our random enemy at our spawn point
            enemy.transform.parent = room.transform; // set our transform to parent
        }
    }

    // Function to spawn the player
    private void SpawnPlayer()
    {
        int roomX = UnityEngine.Random.Range(0, cols); // random col
        int roomY = UnityEngine.Random.Range(0, rows); // random row

        Vector3 spawnPosition = grid[roomX, roomY].playerSpawnPoints[0].transform.position; // get our player spawn point

        GameObject playerTank = Instantiate(playerPrefab, spawnPosition, Quaternion.identity);
        playerData = playerTank.GetComponent<TankData>(); // set our tank data
        playerTransform = playerTank.transform; // set the player transform

        // set our camera follow taret
        CameraFollow cf = mainCamera.GetComponent<CameraFollow>();
        cf.myTarget = playerTransform;

    }

    // Helper function to convert a datetime to an int
    private int DateToInt(DateTime dateTime)
    {
        return dateTime.Year + dateTime.Month + dateTime.Day + dateTime.Hour + dateTime.Minute + dateTime.Second + dateTime.Millisecond;
    }
}
