﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour {

    public List<Powerup> powerUps; // list of current powerups on the tank

    private TankData data; // Our tank data

	// Use this for initialization
	void Start () {
        data = gameObject.GetComponent<TankData>();
        powerUps = new List<Powerup>();
	}
	
	// Update is called once per frame
	void Update () {

        List<Powerup> expiredPowerups = new List<Powerup>(); // temp array to hold our expired powerups

        // Go through all of our powerups and update time/deactivate if necessary
		foreach (Powerup powerUp in powerUps)
        {
            powerUp.duration -= Time.deltaTime; // update the timer of each of our powerups

            if (powerUp.duration <= 0)
                expiredPowerups.Add(powerUp); // add this powerup to the list of expired powerups
                
        }

        // Next loop through and remove any powerups with a duration of <= 0
        foreach (Powerup powerUp in expiredPowerups)
        {
            if (powerUp.duration <= 0)
            {
                powerUp.OnDeactivate(data); // Remove the powerup effect
                powerUps.Remove(powerUp); // remove the powerup from the list
            }
        }
	}

    // Funciton to add powerup to our list
    public void Add (Powerup powerUp)
    {
        powerUp.OnActivate(data); // Activate the powerupt
        if (!powerUp.isPermanent)
            powerUps.Add(powerUp); // add it to our list if its not permanent
    }
}
