﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject pickupPrefab; // hold what we spawn
    public float spawnDelay; // time to wait before spawning another pickup

    private float nextSpawnTime; // timer to tell if we should spawn another
    private Transform myTransform; // variable for our transform
    private GameObject spawnedPickup; // variable to hold the currently spawned object

	// Use this for initialization
	void Start () {
        myTransform = transform; // assign our transform
        nextSpawnTime += Time.time + spawnDelay; // Set the first time we can respawn
	}
	
	// Update is called once per frame
	void Update () {

        // check if its time to spawn a pickup
        if (!spawnedPickup)
        {
            if (Time.time > nextSpawnTime)
            {
                spawnedPickup = Instantiate(pickupPrefab, myTransform.position, Quaternion.identity); // spawn pickup
                nextSpawnTime = Time.time + spawnDelay; // reset our spawn timer
                spawnedPickup.transform.parent = myTransform; // set our parent transform
            }
        }
        else
            nextSpawnTime = Time.time + spawnDelay; // keep reseting our next spawn time
	}
}
