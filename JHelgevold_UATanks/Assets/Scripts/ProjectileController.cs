﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {

    private int myDamage; // Reference to the damage done by this projectile
    private GameManager gm; // Reference to GM
    private string myShooter; // Name of who shot this buillet
    public Rigidbody myRigidBody; // Reference to the rigidboyd of this projectile

	// Use this for initialization
	void Start () {
        gm = FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    // Funciton to fire our instantiated bullet
    public void Fire(int damage, float speed, float lifetime, string shooter)
    {
        Destroy(gameObject, lifetime); // Set our expiration
        myRigidBody.AddForce(transform.forward * speed, ForceMode.VelocityChange); // send our bullet on its way via the rigidbody
        myDamage = damage; // Set our bullet damage
        myShooter = shooter; // Set our shooter
    }

    // Handle collisions since this is a trigger
    private void OnTriggerEnter(Collider other)
    {
        // we dont want our bullet colliding with our listening sphere
        if (other.isTrigger)
            return;

        // If we hit a player or enemy tank
        if (other.gameObject.layer == gm.playerLayer || other.gameObject.layer == gm.enemyLayer)
            other.gameObject.SendMessage("TakeDamage", myDamage); // Tell the player to take damage

        Destroy(gameObject); // Destroy ourselves
    }
}
