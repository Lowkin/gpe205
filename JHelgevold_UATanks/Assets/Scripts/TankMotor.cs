﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))] // Require a TankData component if a motor is added

public class TankMotor : MonoBehaviour {

    private CharacterController characterController; // Character Controller component
    private Transform myTransform; // Reference for our transform object 
    private TankData data; // Reference for our tank data
    private GameManager gm; // Reference to the Game Manager object

    public int currentHitPoints; // Variable to hold this tanks current health.  This is only public until UI support for displaying it is ready

    // Use this for initialization
    void Start () {
        characterController = gameObject.GetComponent<CharacterController>(); // get reference to our Character Controller component
        data = gameObject.GetComponent<TankData>(); // assign our tank data reference
        myTransform = transform; // assign our transform reference
        currentHitPoints = data.hitPoints; // set our starting hit points
        gm = FindObjectOfType<GameManager>(); // assign our game manager object
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Function to move our tank
    public void Move(float speed)
    {
        characterController.SimpleMove(myTransform.forward * speed); // Pass our transforms forward vecotr multiplied by our given speed to the simple move function of CharacterController
    }

    // Function to rotate our tank
    public void Rotate(float speed)
    {
        // Rotate transform around the up vector taking into account the provided speed and the time delta for this frame
        myTransform.Rotate(Vector3.up * speed * Time.deltaTime, Space.World);
    }

    // Function to rotate towards a target
    public void RotateTowards(Vector3 target, float speed)
    {
        // Rotate transform towards the target
        myTransform.rotation = Quaternion.RotateTowards(myTransform.rotation, Quaternion.LookRotation(target.normalized), Time.deltaTime * speed);
    }

    // Function to take damage
    public void TakeDamage(int damage)
    {
        currentHitPoints -= damage; // update our hps

        // If it was a fatal blow
        if (currentHitPoints <= 0)
        {
            // if this tank is the player; game over man
            if (gameObject.layer == gm.playerLayer)
                gm.GameOver();
            else
            {
                gm.UpdatePlayerScore(data.pointValue); // update the player score for destroying this tank
                Destroy(gameObject); // otherwise we destroy this tank
            }
        }
        
    }
}
