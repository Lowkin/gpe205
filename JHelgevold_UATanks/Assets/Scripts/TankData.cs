﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {

    public float forwardSpeed = 3; // variable to hold our forward speed
    public float backwardSpeed = 3; // variable to hold our backward speed
    public float turnSpeed = 180; // variable to hold our rotation speed
    public int hitPoints = 50; // Number of hitpoints this tank has
    public int maxHitPoints = 50; // Number of max hitpoints the tank can have
    public int pointValue = 5; // Number of points this tank is worth when destroyed
    public int damageValue = 25; // Amount of damage this tank does when hitting with a bullet
    public float fireDelay = 1.5f; // Ammount of time to wait before being able to fire again
    public float projectileSpeed = 5f; // Speed at which the tank fires the projectile
    public int projectileLife = 5; // How long the bullet stays alive if it doesnt hit anything

    public GameObject bullet; // Reference to store the bullet type this tank will fire
    public Transform bulletSource; // Reference to the location a bullet is shot from

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
