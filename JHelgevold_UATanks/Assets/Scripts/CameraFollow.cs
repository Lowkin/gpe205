﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Camera myCamera; // Camera reference
    public Transform myTarget; // Target for camera to folow

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        myCamera.transform.position = new Vector3(myTarget.position.x, myCamera.transform.position.y, myTarget.position.z);
	}
}
