﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))] // Require a TankData component if a motor is added

public class TankShooter : MonoBehaviour {

    private TankData data; // Reference for our tank data

    public Transform cannonTransform; // Variable to hold reference to our cannon for aiming

    // Use this for initialization
    void Start () {
        data = gameObject.GetComponent<TankData>(); // Get our tank data info
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // Function to shoot a bullet
    public void Shoot()
    {
        ProjectileController bullet = Instantiate(data.bullet, data.bulletSource.position, cannonTransform.rotation).GetComponent<ProjectileController>(); // Get reference to our newly shot bullet
        bullet.Fire(data.damageValue, data.projectileSpeed, data.projectileLife, data.name); // Fire our bullet
    }

    // Function to aim turret
    public void Aim(Vector3 aimTarget, bool playerOverride)
    {
        // If this is a player we auto aim where the target is
        if (playerOverride) {
            Quaternion targetRotation = Quaternion.LookRotation(aimTarget - cannonTransform.position); // Build a quaternion with the look direction we want
            targetRotation.x = 0; // remove any x rotation
            targetRotation.z = 0; // remove any z rotation

            cannonTransform.rotation = targetRotation; // Set our new rotation
        }
        // Else we are an AI and we will rotate our turret at turn speed
        else
            cannonTransform.rotation = Quaternion.RotateTowards(cannonTransform.rotation, Quaternion.LookRotation((aimTarget - cannonTransform.position).normalized), Time.deltaTime * data.turnSpeed);
    }
}
