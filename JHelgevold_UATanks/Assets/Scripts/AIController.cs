﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    private Transform myTransform; // Store a reference to our transform
    public Transform cannonTransform; // Variable to hold reference to our cannon for aiming
    private GameManager gm; // GameManager reference

    private bool canFire = true; // Bool to tell if we can fire again
    private float lastFireTime = 0; // Float to hold the last time we fired

    public TankMotor motor; // Reference for our tank motor object
    public TankData data; // Reference to a tank data object that holds this tanks data info
    public TankShooter shooter; // Reference to a tank shooter object

    public SphereCollider listenSphere; // Reference to our listening collider

    /* Navigation Variables */
    private int avoidanceStage = 0; // Variable to store what phase of obstacle avoidance we are in
    private float exitTime; // Variable to hold how long we have left in our avoidance state
    private int avoidanceDirection = -1; // Int to store which direction we are using to avoid obstacles
    private int currentDestination = 0; // Variable to hold the current destination our tank is moving to
    private Room myRoom; // Room this tank was instantiated into

    public Vector3 target; // Transform of the location we are attempting to move
    public float avoidanceTime = 2f; // Variable to store how long we will try to avoid an object
    public enum AttackMode { Chase, Patrol, Watcher, HVT }; // Enum to allow behavior selection of this AI unit
    public AttackMode attackMode; // Attack mode selected for this controller

    public GameObject[] waypoints; // Array to hold waypoints the Patrol tank will follow
    public float continueDistance; // Variable to hold how close to a waypoint we get before targetting the next waypoint
    /************************/

    /* Vision and Shooting Variables */
    public float fovVision; // Variable to hold the visual FOV value
    public float fovFire; // Variable to hold the fire turrent FOV value
    public float hearDistance; // Variable to hold how far away the tank can hear the player

    private bool targetSpotted = false;  // Boolean to know if we've spotted a target
    private bool targetAcquired = false; // Boolean to know if we have a target to shoot at
    /************************/

    /* Visual accessors */
    public Renderer tankRenderer;
    public Renderer cannonRenderer;
    public Renderer barrelRenderer;


    // Use this for initialization
    void Start () {

        myTransform = transform; // Store a reference to our transform
        gm = GameObject.FindObjectOfType<GameManager>(); // get our game manager reference
        listenSphere.radius = hearDistance; // Set our radius to our listening distance

        // Assign our tank colors
        switch (attackMode)
        {
            case AttackMode.HVT:
                tankRenderer.material = gm.enemyMaterials[1];
                cannonRenderer.material = gm.enemyMaterials[1];
                barrelRenderer.material = gm.enemyMaterials[1];
                break;
            case AttackMode.Chase:
                tankRenderer.material = gm.enemyMaterials[0];
                cannonRenderer.material = gm.enemyMaterials[0];
                barrelRenderer.material = gm.enemyMaterials[0];
                break;
            case AttackMode.Watcher:
                tankRenderer.material = gm.enemyMaterials[3];
                cannonRenderer.material = gm.enemyMaterials[3];
                barrelRenderer.material = gm.enemyMaterials[3];
                break;
            case AttackMode.Patrol:
                tankRenderer.material = gm.enemyMaterials[2];
                cannonRenderer.material = gm.enemyMaterials[2];
                barrelRenderer.material = gm.enemyMaterials[2];
                break;
        }
        // set our room object
        myRoom = myTransform.GetComponentInParent<Room>();

        if (waypoints.Length == 0)
            waypoints = myRoom.waypoints; // set our waypoints to the room waypoints if they dont exist
    }
	
	// Update is called once per frame
	void Update () {
        //shooter.cannonTransform.LookAt(gm.playerTransform);

        // Check our fire ability
        if (Time.time - lastFireTime >= data.fireDelay)
            canFire = true;

        // Begin state machine for AI behavior

        if (attackMode == AttackMode.HVT)
        {
            // If we have seen the player
            if (targetSpotted)
            {
                // If we are trying to avoid an obstacle
                if (avoidanceStage != 0)
                {
                    DoAvoidance(); // Run our avoidance algorithm
                }
                else
                {
                    target = (-1 * (gm.playerTransform.position - myTransform.position)).normalized + myTransform.position; // get our flee direction
                    DoMove(); // run away
                    AimAndFire(); // Aim and Fire
                }
            }
        }
        // If we are in a chase mode
        else if (attackMode == AttackMode.Chase)
        {
            // If we have seen a target
            if (targetSpotted)
            {
                target = gm.playerTransform.position; // set our target
                // If we are trying to avoid an obstacle
                if (avoidanceStage != 0)
                {
                    DoAvoidance(); // Run our avoidance algorithm
                }
                else
                {
                    DoMove(); // Run our chase algorithm
                    // Aim and Fire
                    AimAndFire();
                }
            }
        }
        // If we are on patrol
        else if (attackMode == AttackMode.Patrol)
        {            
            // If we have not seen an enemy
            if (targetSpotted)
            {
                target = gm.playerTransform.position; // set our target
                AimAndFire(); // Aim and fire       
            }
            else
            {
                target = waypoints[currentDestination].transform.position;
                // If we are trying to avoid an obstacle
                if (avoidanceStage != 0)
                {
                    DoAvoidance(); // Run our avoidance algorithm
                }
                // If we are close enough to the next waypoint to change targets do it
                else if (Vector3.Distance(myTransform.position, waypoints[currentDestination].transform.position) <= continueDistance)
                {
                    currentDestination = currentDestination == waypoints.Length - 1 ? 0 : currentDestination + 1;
                }
                // otherwise we move to the next target
                else 
                {
                    DoMove();
                }
            }

        }
        // If we are set to watch
        else if (attackMode == AttackMode.Watcher)
        {
            // If we have not seen an enemy
            if (targetSpotted)
            {
                target = gm.playerTransform.position; // set our target
                AimAndFire(); // Aim and fire       
            }
        }
    }

    // Function to hold our obstacle avoidance algorithm
    void DoAvoidance()
    {
        switch (avoidanceStage)
        {
            case 1:
                // Rotate left a unit to see if we can then avoid the obstacle

                motor.Rotate(avoidanceDirection * data.turnSpeed);

                // Check if we can move forward now
                if (CanMove(data.forwardSpeed))
                    avoidanceStage = 2; // transition to next avoidance stage

                exitTime = avoidanceTime; // set our designer designed time to stay in stage two
                break;
            case 2:
                // Move ahead if we can
                if (CanMove(data.forwardSpeed))
                {
                    exitTime -= Time.deltaTime; // update our timer
                    motor.Move(data.forwardSpeed); // move the tank forward

                    // If we have hit the limit for how long we will be in this stage
                    if (exitTime <= 0)
                    {
                        avoidanceStage = 0;
                    }
                }
                // Otherwise we loop back to stage 1 of avoidance and keep turning till we can
                else
                {
                    avoidanceStage = 1;
                }

                break;
        }
    }

    // Function to hold our chase algorithm
    void DoMove()
    {
        //Debug.Log(Vector3.SignedAngle(target.position - myTransform.position, myTransform.forward,  myTransform.up));
        // Rotate our tank towards the target location
        motor.RotateTowards(target - myTransform.position, data.turnSpeed);

        // If we can move in the direction we are facing we start moving
        if (CanMove(data.forwardSpeed))
        {
            motor.Move(data.forwardSpeed);
        }
        // otherwise we attempt to avoid
        else
        {
            //ChooseDirection();
            avoidanceStage = 1;
        }

    }

    // Function to check if we can move in our current direction
    bool CanMove(float speed)
    {
        RaycastHit hit; // raycast object to store our hit information

        // Cast a ray from the enemy tank in its forward direction but only test against layer 11 (obstacles) and 9 (enemies)
        if (Physics.Raycast(myTransform.position, myTransform.forward, out hit, speed, 1 << 11 | 1 << 9))
        {
            return false; // We hit something so we cant move forward
        }

        return true; // Nothing was hit so we can go forward
    }

    // Function to chose which direction to rotate to try and go to our target
    void ChooseDirection()
    {
        // Chose direction to rotate based on the angle between the forward vector and the vector to the target
        if (Vector3.SignedAngle(myTransform.forward, target - myTransform.position, myTransform.up) < 0)
            avoidanceDirection = -1;
        else
            avoidanceDirection = 1;
    }

    // Function to handle 'hearing' and then 'seeing' player
    private void OnTriggerStay(Collider other)
    {
        Transform otherTransform = other.transform; // Store a reference to the transform
        Vector3 targetDirection = other.transform.position - cannonTransform.position;
        // If a player has come into our 'listening' circle
        if (other.gameObject.layer == gm.playerLayer)
        {
            // If we hit our player object
            RaycastHit hit;
            if(Physics.Raycast(myTransform.position, targetDirection, out hit))
            {
                if (hit.collider.gameObject.layer == gm.playerLayer)
                {
                    shooter.Aim(otherTransform.position, false); // Always aim at our target

                    float fovAngle = Vector3.Angle(targetDirection, cannonTransform.forward); // Our angle to the target
                    if (fovAngle < fovFire)
                    {
                        targetSpotted = true; // We can see our target
                        targetAcquired = true; // We can fire at our target
                    }
                    else if (fovAngle < fovVision)
                    {
                        targetSpotted = true; // We can see our target
                        targetAcquired = false; // but we cant fire yet
                    }
                    else
                    {
                        targetSpotted = false; // We cant see our target yet
                        targetAcquired = false; // We cant fire at our target yet
                    }
                }
            }
        }
    }
    // Function to reset our targetting variables when the player tank leaves detection
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == gm.playerLayer)
        {
            targetSpotted = false; // We cant see our target yet
            targetAcquired = false; // We cant fire at our target yet
        }
    }

    // Funciton to instruct our shooter object to shoot
    private void AimAndFire()
    {
        shooter.Aim(target, false); // Aim at our player

        if (canFire && targetAcquired) // If we have acquired the target we shoot
        {
            shooter.Shoot();
            lastFireTime = Time.time;
            canFire = false;
        }
    }
}
