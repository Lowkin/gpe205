﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    private Transform myTransform; // Store a reference to our transform
    private bool canFire = true; // Bool to tell if we can fire again
    private float lastFireTime = 0; // Float to hold the last time we fired
    private GameManager gm; // Reference to the Game Manager object

    public enum InputScheme {WASD, arrowKeys}; // enum holding the available input schemes 
    public InputScheme input = InputScheme.WASD; // set our input scheme to WASD by default

    public TankMotor motor; // Reference for our tank motor object
    public TankData data; // Reference to a tank data object that holds this tanks data info
    public TankShooter shooter; // Reference to a tank shooter object

    // Use this for initialization
    void Start () {

        myTransform = transform; // Store a reference to our transform

        // If motor isnt specified in the inspector; set it
        if (!motor)
            motor = myTransform.GetComponent<TankMotor>();

        // If data isnt specified in the inspector; set it
        if (!data)
            data = myTransform.GetComponent<TankData>();

        // If shooter isnt specified in the inspector; set it
        if (!shooter)
            shooter = myTransform.GetComponent<TankShooter>();

        gm = FindObjectOfType<GameManager>(); // assign our game manager object
    }
	
	// Update is called once per frame
	void Update () {

        // Check our fire ability
        if (Time.time - lastFireTime >= data.fireDelay)
            canFire = true;

        switch(input)
        {
            // Check if we are using arrow keys
            case InputScheme.arrowKeys:
                if (Input.GetKey(KeyCode.UpArrow)) // Go Forward
                {
                    motor.Move(data.forwardSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow)) // Go Backward
                {
                    motor.Move(-data.backwardSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow)) // Turn Right
                {
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow)) // Turn Left
                {
                    motor.Rotate(-data.turnSpeed);
                }
                break;
            // Check if we are using WASD
            case InputScheme.WASD:
                if (Input.GetKey(KeyCode.W)) // Go Forward
                {
                    motor.Move(data.forwardSpeed);
                }
                if (Input.GetKey(KeyCode.S)) // Go Backward
                {
                    motor.Move(-data.backwardSpeed);
                }
                if (Input.GetKey(KeyCode.D)) // Turn Right
                {
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.A)) // Turn Left
                {
                    motor.Rotate(-data.turnSpeed);
                }
                break;
        }
        // If we press space; fire a bullet
        if (Input.GetKey(KeyCode.Space) && canFire)
        {
            shooter.Shoot();
            lastFireTime = Time.time;
            canFire = false;
        }

        // If we are clicking the left mouse button aim the cannon
        if (Input.GetMouseButton(0))
        {
            float distance = Vector3.Distance(gm.mainCamera.transform.position, myTransform.position); // get the distance from the camera the tank is
            Vector3 aimTarget = Input.mousePosition; // set the aim target to where the mouse is
            aimTarget.z = distance; // set the z value of the aim target the distance between the camera and the tank so that the conversion will give the world spot on the same plane the tank is on

            aimTarget = gm.mainCamera.ScreenToWorldPoint(aimTarget); // get our final aim location
            shooter.Aim(aimTarget, true); // Aim our cannon
        }
		
	}
}
